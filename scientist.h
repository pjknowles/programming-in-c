#ifndef PROGRAMMING_IN_C_SCIENTIST_H
#define PROGRAMMING_IN_C_SCIENTIST_H
#include <string>
#include <vector>
/*!
 * @brief A container for holding and describing information about an individual
 */
class scientist {
 public:
  /*!
   * @param who the name of the individual
   * @param years his or her current age
   */
  scientist(std::string who = "anonymous", int years = 99);
  /*!
   * The name of the scientist
   */
  std::string name;
  /*!
   * @return Whether he is at least 18
   */
  bool can_be_served_in_the_pub();
  /*!
   * @return  Whether he is at least 60
   */
  bool has_a_bus_pass();
  bool has_a_PhD;
  void record_library_fine(double amount) {library_fines.push_back(amount);}
  double total_library_fines () const;
  size_t number_library_fines () const { return library_fines.size();}
 private:
  std::vector<double> library_fines;
  int age;
};

#endif //PROGRAMMING_IN_C_SCIENTIST_H
