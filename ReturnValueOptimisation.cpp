//
// Created by Peter Knowles on 27/09/2018.
//

#include <iostream>
#include <string>

class thing {
 public:
  thing() : buffer("I am a constructed thing") {}
  thing(const thing&) : buffer("I am a copy-constructed thing") { std::cout << "A copy was made.\n"; }
  std::string buffer;
};

thing f() {
  return thing();
}

int main() {
  thing obj = f();
  std::cout << obj.buffer << std::endl;
  return 0;
}