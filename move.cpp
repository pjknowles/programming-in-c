//
// Created by Peter Knowles on 27/09/2018.
//

#include <iostream>
#include <string>
#include <vector>

class thing {
 public:
  thing() : buffer("I am a constructed thing") { std::cout << "Constructor" << std::endl; }
  thing(const thing& source) : buffer("I am a copy-constructed thing") {  }
  thing(const thing&& source) : buffer("I am a move-constructed thing") {  }
  std::string buffer;
};

int main() {
  std::vector<thing> things;
  things.emplace_back();
  std::cout << things.back().buffer << std::endl;
  auto copied = things.back();
  std::cout << copied.buffer << std::endl;
  things.push_back(copied);
  things.push_back(std::move(copied));
  std::cout << things.back().buffer << std::endl;

  std::vector<int> ints;
  ints.push_back(1);
  ints.push_back(2);
  std::cout << "ints address: " << ints.data() << ", empty: " << ints.empty() << ", size: " << ints.size() << std::endl;
  std::vector<std::vector<int>> holder;
  auto ints2 = std::move(ints);
  std::cout << "ints2 address: " << ints2.data() << ", empty: " << ints2.empty() << ", size: " << ints2.size()
            << std::endl;
  std::cout << "ints address: " << ints.data() << ", empty: " << ints.empty() << ", size: " << ints.size() << std::endl;
  holder.push_back(std::move(ints2));
  std::cout << "holder.back() address: " << holder.back().data() << ", empty: " << holder.back().empty() << ", size: "
            << holder.back().size() << std::endl;
  std::cout << "ints2 address: " << ints2.data() << ", empty: " << ints2.empty() << ", size: " << ints2.size()
            << std::endl;
  std::cout << "ints address: " << ints.data() << ", empty: " << ints.empty() << ", size: " << ints.size() << std::endl;
  return 0;
}