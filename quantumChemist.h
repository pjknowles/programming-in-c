#ifndef PROGRAMMING_IN_C_QUANTUMCHEMIST_H
#define PROGRAMMING_IN_C_QUANTUMCHEMIST_H
#include <string>
#include "scientist.h"
/*!
 * @brief A container for holding and describing information about an individual
 */
class quantumChemist : public scientist {
 public:
  /*!
   * @param who the name of the individual
   * @param years his or her current age
   * @param h what this person thinks is the value of Planck's constant
   */
  quantumChemist(std::string who = "anonymous", int years = 99, double h=0);
  /*!
   * Whether this quantum chemist has shown that he knows what Hartree-Fock theory is
   */
  bool demonstrated_hf_proficiency;
  /*!
   * @return  the number of digits of Planck's constant that are known
   */
  int digits_known_of_h();
 private:
  double memorized_value_of_h;
};

#endif //PROGRAMMING_IN_C_QUANTUMCHEMIST_H
