//
// Created by Peter Knowles on 08/10/2018.
//
#include <string>
#include <iostream>

template<class T>
T& myMax(T& a, T& b) {
  return a > b ? a : b;
}

template<class keyType, class valueType>
class myPair {
  keyType key;
  valueType value;
 public:
  myPair() = delete;
  myPair(keyType k, valueType v) : key(std::move(k)), value(std::move(v)) {}
  std::string str() { return std::to_string(key) + ":" + value; }
};

int main() {
  int i = 1, j = 2;
  double a = 1.5, b = 2.5;
  std::cout << myMax(i, j) << std::endl;
  std::cout << myMax(a, b) << std::endl;
  myMax(i, j) = 99;
  std::cout << i << " " << j << std::endl;

  myPair<int,std::string> p(99,"ninety-nine");
  std::cout << p.str() << std::endl;
}
