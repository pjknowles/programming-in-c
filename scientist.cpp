#include <iostream>
#include <numeric>
#include "scientist.h"
scientist::scientist(std::string who, int years) : name(who), age(years), has_a_PhD(false) {}
bool scientist::can_be_served_in_the_pub() { return age >= 18; }
bool scientist::has_a_bus_pass() { return age >= 60; }

double scientist::total_library_fines() const {
//  double total = 0;
//  for (auto i=0; i<library_fines.size(); ++i)
//    total += library_fines[i];
//  for (std::vector<double>::const_iterator f = library_fines.begin();
//  for (auto f = library_fines.cbegin();
//       f != library_fines.cend();
//       f++)
//    total += *f;
//  for (const auto &f : library_fines)
//    total += f;
//  return total;
  return std::accumulate(library_fines.cbegin(),library_fines.cend(),0.0);
}