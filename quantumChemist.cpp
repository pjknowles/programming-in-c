#include <iostream>
#include "quantumChemist.h"
#include <cmath>
quantumChemist::quantumChemist(std::string who, int years, double h)
    : scientist(who, years), memorized_value_of_h(h), demonstrated_hf_proficiency(false) {}
int quantumChemist::digits_known_of_h() {
  double error = std::fabs(memorized_value_of_h / double{6.62607004e-34} - 1);
  return -std::log10(error);
}