#include <iostream>
#include "quantumChemist.h"

void printName(const scientist& person) {
  std::cout << "A scientist called " << person.name <<" has "<<person.number_library_fines()<<" library fines totalling £"<<person.total_library_fines() << std::endl;
}

int main() {
  quantumChemist Peter;
  Peter.name = "Peter"; Peter.has_a_PhD = true;

  quantumChemist* Marat;
  {
    Marat = new quantumChemist("Marat", 17, 6.63e-34);
    Marat->has_a_PhD = true;
    Marat->demonstrated_hf_proficiency = true;
  }

  Peter.record_library_fine(0.32);
  Peter.record_library_fine(4.81);
  Peter.record_library_fine(5632.01);
  printName(Peter);
  printName(*Marat);

  delete Marat;

  return 0;
}